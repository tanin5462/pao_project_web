import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import vueNumeralFilterInstaller from 'vue-numeral-filter';

Vue.use(VueRouter)
Vue.use(vueNumeralFilterInstaller);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyAPaZqipWhipFcPmKQOFigtVdN24-Cetdw",
  authDomain: "pao-my-web.firebaseapp.com",
  databaseURL: "https://pao-my-web.firebaseio.com",
  projectId: "pao-my-web",
  storageBucket: "",
  messagingSenderId: "998190381130",
  appId: "1:998190381130:web:14a61f257ebad3c2584eb3"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();

Vue.mixin({
  data() {
    return {
      appVersion: "1.0.4"
    }
  },
})


export default function ( /* { store, ssrContext } */ ) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      x: 0,
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}
