const routes = [{
    path: '/',
    component: () => import('pages/login.vue'),
    name: 'login'

  },
  {
    path: '',
    component: () => import('layouts/MyLayout.vue'),
    children: [{
        path: '/usermain',
        component: () => import('pages/usermain.vue')
      },
      {
        path: '/customermain',
        component: () => import('pages/customermain.vue')
      },
      {
        path: '/systemlog',
        component: () => import('pages/systemlog.vue')
      },
      {
        path: '/summary',
        component: () => import('pages/summary.vue')
      },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
